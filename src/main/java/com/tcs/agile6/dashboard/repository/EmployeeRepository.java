package com.tcs.agile6.dashboard.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.tcs.agile6.dashboard.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, String> {

	List<Employee> findByIOU(String IOU);
	List<Employee> findByprojectType(String projectType);
	List<Employee> findByagComp(String agComp);
}
