package com.tcs.agile6.dashboard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.agile6.dashboard.model.Employee;
import com.tcs.agile6.dashboard.service.WorkForceService;

@RestController
public class WorkForceController {
	
	@Autowired
	WorkForceService workForceService;
	
	@RequestMapping(value="/login", method=RequestMethod.GET)
		public List<Employee> login() {
		
		return workForceService.getEmployeeByIOU();
		
		
		
			
		}
}
