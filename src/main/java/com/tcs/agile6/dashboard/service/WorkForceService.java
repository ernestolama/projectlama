package com.tcs.agile6.dashboard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tcs.agile6.dashboard.model.Employee;
import com.tcs.agile6.dashboard.repository.EmployeeRepository;

@Component
public class WorkForceService {

	@Autowired
	EmployeeRepository empRepo; 
	
	public List<Employee> getEmployeeByIOU() {
		
		return empRepo.findByIOU("");
	}
public List<Employee> findByprojectType() {
		
		return empRepo.findByprojectType("");
	}

public List<Employee> findByfindByagComp() {
	return empRepo.findByagComp("");
}
}
