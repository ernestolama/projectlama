package com.tcs.agile6.dashboard.model;

import java.util.Date;

import javax.persistence.Entity;

@Entity
public class Employee {

		String empId;
		String name;
		String IOU;
		String subIOU;
		String projectName;
		String projectType;
		String projectId;
		String agComp;
		String TFac;
		String geog;
		String depRegion;
		String depCity;
		Date compDate;
		String prevTFactor;
		public String getEmpId() {
			return empId;
		}
		public String getProjectType() {
			return projectType;
		}
		public void setProjectType(String projectType) {
			this.projectType = projectType;
		}
		public void setEmpId(String empId) {
			this.empId = empId;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getIOU() {
			return IOU;
		}
		public void setIOU(String iOU) {
			IOU = iOU;
		}
		public String getSubIOU() {
			return subIOU;
		}
		public void setSubIOU(String subIOU) {
			this.subIOU = subIOU;
		}
		public String getProjectName() {
			return projectName;
		}
		public void setProjectName(String projectName) {
			this.projectName = projectName;
		}
		public String getProjectId() {
			return projectId;
		}
		public void setProjectId(String projectId) {
			this.projectId = projectId;
		}
		public String getAgComp() {
			return agComp;
		}
		public void setAgComp(String agComp) {
			this.agComp = agComp;
		}
		public String getTFac() {
			return TFac;
		}
		public void setTFac(String tFac) {
			TFac = tFac;
		}
		public String getGeog() {
			return geog;
		}
		public void setGeog(String geog) {
			this.geog = geog;
		}
		public String getDepRegion() {
			return depRegion;
		}
		public void setDepRegion(String depRegion) {
			this.depRegion = depRegion;
		}
		public String getDepCity() {
			return depCity;
		}
		public void setDepCity(String depCity) {
			this.depCity = depCity;
		}
		public Date getCompDate() {
			return compDate;
		}
		public void setCompDate(Date compDate) {
			this.compDate = compDate;
		}
		public String getPrevTFactor() {
			return prevTFactor;
		}
		public void setPrevTFactor(String prevTFactor) {
			this.prevTFactor = prevTFactor;
		}
		
		
		
}
